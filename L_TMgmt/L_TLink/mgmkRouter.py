from flask import Blueprint, jsonify, request, json
import orgMgmt.orgOperation.subcomOprt as subcomOprt
from L_TMgmt.L_TOperation import stationOprt, lineOprt, mgmkOprt

milemark = Blueprint('milemark', __name__)


@milemark.route('/', methods=['GET'])
def index():
    subcomOprt.hello()
    print("here is milemark")
    return jsonify('milemarkkkkkk')


# ###########################################################################################################登录
@milemark.route('/init', methods=['GET'])
def init():
    if mgmkOprt.getAllMark() == 0:
        # 待测试
        return jsonify(0)
    else:
        marklist = []
        for mark in mgmkOprt.getAllMark():
            marklist.append({"milemark_id": mark[0], "milemark_name": mark[1],
                             "line_id": mark[2], "line_name": mark[3],
                             "start_id": mark[4], "start_name": mark[5],
                             "stop_id": mark[6], "stop_name": mark[7]})
        print("里程标列表")
        print(marklist)
    return jsonify(marklist)


@milemark.route('/newmilemark', methods=['POST'])
def newline():
    print("new a milemark")
    data = request.get_data()
    print(data)
    json_data = json.loads(data.decode("utf-8"))
    print(json_data)

    if mgmkOprt.newmilemark(json_data.get('milemark_id'), json_data.get('milemark_name'),
                            json_data.get('line_id'), json_data.get('start_point_id'),
                            json_data.get('end_point_id')):
        return jsonify(1)
    else:
        return jsonify(2)


@milemark.route('/editmilemark', methods=['POST'])
def editmilemark():
    print("edit a milemark")
    data = request.get_data()
    print(data)
    json_data = json.loads(data.decode("utf-8"))
    print(json_data)

    if mgmkOprt.editmilemark(json_data.get('milemark_id'), json_data.get('milemark_name'),
                            json_data.get('line_id'), json_data.get('start_point_id'),
                            json_data.get('end_point_id')):
        return jsonify(1)
    else:
        return jsonify(2)


@milemark.route('/select', methods=['POST'])
def milemarkVagueSelect():
    data = request.get_data()
    json_data = json.loads(data.decode("utf-8"))
    word = json_data.get('word')

    if mgmkOprt.searchMark(word) == 0:
        # 待测试
        return jsonify(0)
    else:
        marklist = []
        for mark in mgmkOprt.searchMark(word):
            marklist.append({"milemark_id": mark[0], "milemark_name": mark[1],
                             "line_id": mark[2], "line_name": mark[3],
                             "start_id": mark[4], "start_name": mark[5],
                             "stop_id": mark[6], "stop_name": mark[7]})
        print(marklist)
    return jsonify(marklist)


@milemark.route('/delete', methods=['POST'])
def milemarkDelete():
    data = request.get_data()
    print(data)
    word = data.decode("utf-8")
    print(word)

    if mgmkOprt.deleteMilemark(word):
        return jsonify(1)
    else:
        return jsonify(2)


@milemark.route('/options', methods=['GET'])
def options():
    print("to get options")
    if mgmkOprt.getOptions() == 0:
        return jsonify(0)  # 待测试
    else:
        linelist = []
        index = 0

        for line in mgmkOprt.getOptions():
            # 取得线路经过站点列表
            station_id_list = line[2].split("#")
            print(station_id_list)
            stationlist = mgmkOprt.getstations(station_id_list)
            linelist.append({'line_id': line[0], 'line_name': line[1], 'line_index': index, 'stations': stationlist})
            index += 1
        print("！！！线路列表")
        print(linelist)
    return jsonify(linelist)


@milemark.route('/departoptions', methods=['GET'])
def departoptions():
    print("to get department options")

    options = []
    if lineOprt.getdepartOptions() == 0:
        return jsonify(options)  # 待测试
    else:
        for line in lineOprt.getdepartOptions():
            options.append({'value': line[0], 'label': line[1]})
        print(options)
    return jsonify(options)
