from flask import Blueprint, jsonify, request, json
import orgMgmt.orgOperation.subcomOprt as subcomOprt
from L_TMgmt.L_TOperation import stationOprt, lineOprt

line = Blueprint('line', __name__)


@line.route('/', methods=['GET'])
def index():
    subcomOprt.hello()
    print("here is line")
    return jsonify('lineeeeee')


# ###########################################################################################################登录
@line.route('/init', methods=['GET'])
def init():
    if lineOprt.getAllLine() == 0:
        # 待测试
        return 0
    else:
        linelist = []
        for line in lineOprt.getAllLine():
            # 取得线路经过站点列表
            station_id_list = line[4].split("#")
            print(station_id_list)
            stationlist = lineOprt.getstations(station_id_list)

            linelist.append({"line_id": line[0], "line_name": line[1],
                             "attr_depart_name": line[2], "attr_depart_id": line[3],
                             "stations1": stationlist[0], "stations2": stationlist[1],
                             "station_id_list": station_id_list})
        print(linelist)
    return jsonify(linelist)


@line.route('/newline', methods=['POST'])
def newline():
    print("new a station")
    data = request.get_data()
    print(data)
    json_data = json.loads(data.decode("utf-8"))
    print(json_data)

    if lineOprt.newline(json_data.get('line_id'), json_data.get('line_name'),
                        json_data.get('attr_depart_id'), json_data.get('value')):
        return jsonify(1)
    else:
        return jsonify(2)


@line.route('/editline', methods=['POST'])
def editline():
    print("edit a line")
    data = request.get_data()
    print(data)
    json_data = json.loads(data.decode("utf-8"))
    print(json_data)

    if lineOprt.editline(json_data.get('line_id'), json_data.get('line_name'),
                         json_data.get('attr_depart_id'), json_data.get('value')):
        return jsonify(1)
    else:
        return jsonify(2)


@line.route('/select', methods=['POST'])
def lineVagueSelect():
    data = request.get_data()
    json_data = json.loads(data.decode("utf-8"))
    word = json_data.get('word')

    if lineOprt.searchLine(word) == 0:
        # 待测试
        return jsonify(0)
    else:
        linelist = []
        for line in lineOprt.searchLine(word):
            # 取得线路经过站点列表
            station_id_list = line[3].split("#")
            print(station_id_list)
            stationlist = lineOprt.getstations(station_id_list)

            linelist.append({"line_id": line[0], "line_name": line[1],
                             "attr_depart_name": line[2], "stations1": stationlist[0], "stations2": stationlist[1]})
        print(linelist)
    return jsonify(linelist)


@line.route('/delete', methods=['POST'])
def lineDelete():
    data = request.get_data()
    word = data.decode("utf-8")

    if lineOprt.deleteLine(word):
        return jsonify(1)
    else:
        return jsonify(2)


@line.route('/options', methods=['GET'])
def options():
    print("to get options")
    if lineOprt.getOptions() == 0:
        return jsonify(0)  # 待测试
    else:
        stationlist = []

        for line in lineOprt.getOptions():
            stationlist.append({'label': line[0], 'key': line[1], 'pinyin': line[0], "disabled": False})
        print(stationlist)
    return jsonify(stationlist)


@line.route('/departoptions', methods=['GET'])
def departoptions():
    print("to get department options")

    options = []
    if lineOprt.getdepartOptions() == 0:
        return jsonify(options)  # 待测试
    else:
        for line in lineOprt.getdepartOptions():
            options.append({'value': line[0], 'label': line[1]})
        print(options)
    return jsonify(options)
