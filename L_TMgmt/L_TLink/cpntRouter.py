from flask import Blueprint, jsonify, request, json
import L_TMgmt.L_TOperation.cpntOprt as cpntOprt

cpnt = Blueprint('component', __name__)


@cpnt.route('/init', methods=['GET'])
def init():
    if cpntOprt.getAllCpnt() == 0:
        return cpntOprt.getAllCpnt()
    else:
        cpntlist = []
        for line in cpntOprt.getAllCpnt():
            if line[0] == 0:
                cpntlist.append({"type": line[0], "component_id": line[1],
                                 "component_name": line[2], "attr_depart_id": line[3],
                                 "id": line[4], "name": ''})
            if line[0] == 1:
                cpntlist.append({"type": line[0], "component_id": line[1],
                                 "component_name": line[2], "attr_depart_id": line[3],
                                 "id": line[5], "name": line[6]})

            if line[0] == 2:
                cpntlist.append({"type": line[0], "component_id": line[1],
                                 "component_name": line[2], "attr_depart_id": line[3],
                                 "id": line[7], "name": line[8]})
        print(cpntlist)
    return jsonify(cpntlist)


@cpnt.route('/newCpnt', methods=['POST'])
def newCpnt():
    print("new a cpnt")
    data = request.get_data()
    print(data)
    json_data = json.loads(data.decode("utf-8"))

    if cpntOprt.newCpnt(json_data.get('type'), json_data.get('attr_depart_id'), json_data.get('id')):
        return jsonify(1)
    else:
        return jsonify(2)


@cpnt.route('/getdetail', methods=['POST'])
def getdetail():
    print("get some")
    data = request.get_data()
    # data = int.from_bytes(b, byteorder='big', signed=False)
    print(data)

    cpntlist = []

    if data == b'':
        if cpntOprt.getAllTrain() == 0:
            return cpntOprt.getAllTrain()
        else:
            for line in cpntOprt.getAllTrain():
                cpntlist.append({"id": line[0], "name": ''})

    elif data == b'1':
        if cpntOprt.getAllMile() == 0:
            return cpntOprt.getAllMile()
        else:
            for line in cpntOprt.getAllMile():
                cpntlist.append({"id": line[0], "name": line[1]})
    else:
        if cpntOprt.getAllStation() == 0:
            return cpntOprt.getAllStation()
        else:
            for line in cpntOprt.getAllStation():
                cpntlist.append({"id": line[0], "name": line[1]})
    print(cpntlist)
    return jsonify(cpntlist)


@cpnt.route('/getalldpt', methods=['GET'])
def getalldpt():
    if cpntOprt.getAllDepartment() == 0:
        return cpntOprt.getAllDepartment()
    else:
        cpntlist = []
        for line in cpntOprt.getAllDepartment():
            cpntlist.append({"dpt_id": line[0], "dpt_name": line[1]})
        print(cpntlist)
        return jsonify(cpntlist)


@cpnt.route('/select', methods=['POST'])
def cpntVagueSelect():
    data = request.get_data()
    json_data = json.loads(data.decode("utf-8"))
    word = json_data.get('word')

    if cpntOprt.searchComponent(word) == 0:
        return cpntOprt.getAllCpnt()
    else:
        cpntlist = []
        for line in cpntOprt.searchComponent(word):
            if line[0] == 0:
                cpntlist.append({"type": line[0], "component_id": line[1],
                                 "component_name": line[2], "attr_depart_id": line[3],
                                 "id": line[4], "name": ''})
            if line[0] == 1:
                cpntlist.append({"type": line[0], "component_id": line[1],
                                 "component_name": line[2], "attr_depart_id": line[3],
                                 "id": line[5], "name": line[6]})

            if line[0] == 2:
                cpntlist.append({"type": line[0], "component_id": line[1],
                                 "component_name": line[2], "attr_depart_id": line[3],
                                 "id": line[7], "name": line[8]})
        print(cpntlist)
    return jsonify(cpntlist)

@cpnt.route('/delete', methods=['POST'])
def delete():
    data = request.get_data()
    print(data)
    word = data.decode("utf-8")
    print(word)
    if cpntOprt.deleteCpnt(word):
        return jsonify(1)
    else:
        return jsonify(2)
