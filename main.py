import os
from flask import Flask, jsonify, request, json, session
from flask_cors import CORS
from gevent import monkey
from gevent.pywsgi import WSGIServer

from L_TMgmt.L_TLink.lineRouter import line
from L_TMgmt.L_TLink.mgmkRouter import milemark
from L_TMgmt.L_TLink.stationRouter import station
from L_TMgmt.L_TLink.trainRouter import train
from L_TMgmt.L_TLink.cpntRouter import cpnt
from R_CMgmt.riskCopeLink.copeTestRouter import riskCopeTest
from R_CMgmt.riskCopeLink.copeRecordRouter import riskCopeRecord
from R_CMgmt.riskCopeLink.copeAnaRouter import riskCopeAna
from R_CMgmt.riskCopeLink.emergencyRouter import emergency
from accidentMgmt.accidentLink.accRecordRouter import accRecord
from accidentMgmt.accidentLink.accTypeRouter import accType
from orgMgmt.orgLink.dpmtRouter import dpmt
from userMgmt.userLink.userRouter import user
from orgMgmt.orgLink.subcomRouter import subcom
from orgMgmt.orgLink.staffRouter import staff
from orgMgmt.orgLink.opctRouter import ocp
from orgMgmt.orgLink.ocpsRouter import ocps
from riskMgmt.riskLink.riskRouter import risk
from riskMgmt.riskLink.thdRouter import thd
from riskMgmt.riskLink.cstdRouter import std
from riskMgmt.riskLink.recordRouter import record
from userMgmt.userLink.verificationCode import api

monkey.patch_all()

app = Flask(__name__)


app.register_blueprint(user)
app.register_blueprint(subcom, url_prefix='/subcom')
app.register_blueprint(dpmt, url_prefix='/dpmt')
app.register_blueprint(staff, url_prefix='/staff')
app.register_blueprint(ocp, url_prefix='/ocp')
app.register_blueprint(ocps, url_prefix='/ocps')
app.register_blueprint(station, url_prefix='/station')
app.register_blueprint(line, url_prefix='/line')
app.register_blueprint(milemark, url_prefix='/milemark')
app.register_blueprint(cpnt, url_prefix='/component')
app.register_blueprint(train, url_prefix='/train')
app.register_blueprint(risk)
app.register_blueprint(thd)
app.register_blueprint(std)
app.register_blueprint(record)
app.register_blueprint(api)
app.register_blueprint(riskCopeRecord, url_prefix='/riskCopeRecord')
app.register_blueprint(riskCopeTest, url_prefix='/riskCopeTest')
app.register_blueprint(riskCopeAna, url_prefix='/riskCopeAna')
app.register_blueprint(emergency, url_prefix='/emergency')
app.register_blueprint(accType, url_prefix='/accType')
app.register_blueprint(accRecord, url_prefix='/accRecord')
app.secret_key = 'zxdsb'
CORS(app)



# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    app.run()
    http_sever = WSGIServer(('', 5000), app)
    http_sever.serve_forever()
# See PyCharm help at https://www.jetbrains.com/help/pycharm/
