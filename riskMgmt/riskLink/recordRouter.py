from flask import Blueprint, jsonify, request, json
import riskMgmt.riskOperation.recordOprt as recordOprt

import L_TMgmt.L_TOperation.cpntOprt as cpntOprt
import datetime

record = Blueprint('record', __name__)


# 在风险详细页面打开时调用，用于获取某个风险的所有检测记录
@record.route('/selectRecord', methods=['POST'])
def selectRecord():

    data = request.get_data()
    json_data = json.loads(data.decode("utf-8"))
    risk_id = json_data.get('id')
    return recordOprt.selectRec(risk_id)


@record.route('/addRecord', methods=['POST'])
def addRecord():

    data = request.get_data()
    json_data = json.loads(data.decode("utf-8"))
    risk_id = json_data.get('id')
    norm = json_data.get('record_norm')
    return recordOprt.addToData(risk_id, norm)


