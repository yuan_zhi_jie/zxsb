from flask import Blueprint, jsonify, request, json

import riskMgmt.riskOperation.thdOprt as thdOprt
import L_TMgmt.L_TOperation.cpntOprt as cpntOprt
import datetime

thd = Blueprint('thd', __name__)

# 展示某一个风险的所有阈值信息
@thd.route('/selectThreshold', methods=['POST'])
def selectThreshold():

    data = request.get_data()
    json_data = json.loads(data.decode("utf-8"))
    risk_id = json_data.get('id')
    return thdOprt.selectThe(risk_id)

# 删除阈值信息
@thd.route('/deleteThreshold', methods=['POST'])
def deleteThreshold():

    data = request.get_data()
    json_data = json.loads(data.decode("utf-8"))
    threshold_id = json_data.get('threshold_id')
    return thdOprt.onDel(threshold_id)

# 修改阈值信息
@thd.route('/modifyThreshold', methods=['POST'])
def modifyThreshold():

    data = request.get_data()
    json_data = json.loads(data.decode("utf-8"))
    threshold_id = json_data.get('threshold_id')
    range = json_data.get('range')
    norm = json_data.get('norm')
    level = json_data.get('level')
    method = json_data.get('method')
    return thdOprt.modifyThr(threshold_id, range, norm, level, method)

# 新增阈值信息
@thd.route('/addThd', methods=['POST'])
def addThd():

    data = request.get_data()
    json_data = json.loads(data.decode("utf-8"))
    id = json_data.get('id')
    range = json_data.get('range')
    norm = json_data.get('norm')
    level = json_data.get('level')
    method = json_data.get('method')
    attribute = json_data.get('attribute')
    return thdOprt.add(id, attribute, range, norm, level, method)


