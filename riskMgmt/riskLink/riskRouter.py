from flask import Blueprint, jsonify, request, json

import riskMgmt.riskOperation.riskOprt as riskOprt
import L_TMgmt.L_TOperation.cpntOprt as cpntOprt
import datetime

risk = Blueprint('risk', __name__)

# 查询所有风险信息
@risk.route('/riskinfo', methods=['GET'])
def riskinfo():
    return riskOprt.searchFromData()

# 进行模糊查询
@risk.route('/riskVagueSelect', methods=['POST'])
def riskVagueSelect():

    data = request.get_data()
    json_data = json.loads(data.decode("utf-8"))
    word = json_data.get('word')
    return riskOprt.searchSpecData(word)

# 查询可选择的组分表
@risk.route('/cpntname', methods=['GET'])
def cpntname():
    return cpntOprt.searchNamelist()

# 新增风险
@risk.route('/addRisk', methods=['POST'])
def addRisk():

    data = request.get_data()
    json_data = json.loads(data.decode("utf-8"))
    cpnt_name = json_data.get('cpnt_name')
    name = json_data.get('name')
    return riskOprt.addData(cpnt_name, name)

# 展示风险详细信息
@risk.route('/selectDetail', methods=['POST'])
def selectDetail():

    data = request.get_data()
    json_data = json.loads(data.decode("utf-8"))
    risk_id = json_data.get('id')
    return riskOprt.selectDet(risk_id)



# 修改风险详细信息
@risk.route('/modifyDetail', methods=['POST'])
def modifyDetail():
    data = request.get_data()
    json_data = json.loads(data.decode("utf-8"))
    risk_id = json_data.get('id')
    name = json_data.get('name')
    cpnt_name = json_data.get('cpnt_name')
    describe = json_data.get('describe')
    return riskOprt.modifyD(risk_id, name, cpnt_name, describe)

# 选择器修改时展示数据
@risk.route('/cpntData', methods=['POST'])
def cpntData():
    data = request.get_data()
    json_data = json.loads(data.decode("utf-8"))
    name = json_data.get("cpntName")
    return cpntOprt.cpntShow(name)

# 删除风险
@risk.route('/DelRisk', methods=['POST'])
def Delrisk():
    data = request.get_data()
    json_data = json.loads(data.decode("utf-8"))
    risk_id = json_data.get("id")
    return riskOprt.handleDel(risk_id)

