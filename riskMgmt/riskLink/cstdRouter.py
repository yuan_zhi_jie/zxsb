from flask import Blueprint, jsonify, request, json

import riskMgmt.riskOperation.stdOprt as stdOprt
import datetime

std = Blueprint('std', __name__)

# 修改核查规范
@std.route('/modifyGap', methods=['POST'])
def modifyGap():
    data = request.get_data()
    json_data = json.loads(data.decode("utf-8"))
    risk_id = json_data.get('id')
    gap = json_data.get('gap')
    times = json_data.get('time')
    UTC_FORMAT = "%Y-%m-%dT%H:%M:%S.%fZ"
    utcTime = datetime.datetime.strptime(times[0], UTC_FORMAT)
    times[0] = (utcTime + datetime.timedelta(hours=8)).strftime('%Y-%m-%d')
    utcTime = datetime.datetime.strptime(times[1], UTC_FORMAT)
    times[1] = (utcTime + datetime.timedelta(hours=8)).strftime('%Y-%m-%d')
    print(times)
    return stdOprt.modify(risk_id, gap, times[0], times[1])