from flask import Blueprint, jsonify, request, json

from accidentMgmt.accidentOperation import accTypeOprt

accType = Blueprint('accType', __name__)


@accType.route('/', methods=['GET'])
def index():
    accTypeOprt.hello()
    print("here is accType")
    return jsonify('accTypemmm')


# ###########################################################################################################登录
@accType.route('/init', methods=['GET'])
def init():
    if accTypeOprt.getAllaccType() == 0:
        return accTypeOprt.getAllaccType()
    else:
        accTypelist = []
        for line in accTypeOprt.getAllaccType():
            accTypelist.append({"accType_name": line[0], "accType_describe": line[1],
                               "accType_method": line[2]})
        print(accTypelist)
    return jsonify(accTypelist)


@accType.route('/newacc', methods=['POST'])
def newacc():
    print("new a accTypepany")
    data = request.get_data()
    print(data)
    json_data = json.loads(data.decode("utf-8"))
    print(json_data)

    if accTypeOprt.newacc(json_data.get('name'), json_data.get('describe'),json_data.get('method')):
        return jsonify(1)
    else:
        return jsonify(2)


@accType.route('/editacc', methods=['POST'])
def editacc():
    print("new a accTypepany")
    data = request.get_data()
    print(data)
    json_data = json.loads(data.decode("utf-8"))
    print(json_data)

    if accTypeOprt.editsub(json_data.get('name'), json_data.get('describe'),json_data.get('method')):
        return jsonify(1)
    else:
        return jsonify(2)


@accType.route('/select', methods=['POST'])
def accTypeVagueSelect():
    data = request.get_data()
    json_data = json.loads(data.decode("utf-8"))
    word = json_data.get('word')
    accTypelist = []
    if accTypeOprt.searchaccType(word) == 0:
        return jsonify(0)
    else:
        for line in accTypeOprt.searchaccType(word):
            accTypelist.append({"accType_name": line[0], "accType_describe": line[1],
                               "accType_method": line[2]})
    print(accTypelist)
    return jsonify(accTypelist)


@accType.route('/delete', methods=['POST'])
def accTypeDelete():
    data = request.get_data()
    print(data)
    word = data.decode("utf-8")
    print(word)

    if accTypeOprt.deleteaccType(word):
        return jsonify(1)
    else:
        return jsonify(2)


