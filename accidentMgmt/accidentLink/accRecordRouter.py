from flask import Blueprint, jsonify, request, json

from accidentMgmt.accidentOperation import accRecordOprt

accRecord = Blueprint('accRecord', __name__)


@accRecord.route('/', methods=['GET'])
def index():
    accRecordOprt.hello()
    print("here is accType")
    return jsonify('accTypemmm')


# ###########################################################################################################登录
@accRecord.route('/init', methods=['GET'])
def init():
    if accRecordOprt.getAllaccType() == 0:
        return accRecordOprt.getAllaccType()
    else:
        accTypelist = []
        for line in accRecordOprt.getAllaccType():
            time = line[1].strftime('%Y-%m-%d')
            accTypelist.append({"acc_id": line[0], "acc_name": line[2],
                               "acc_type": line[3],"acc_method": line[4],"acc_result": line[5],"acc_time": time})
        print(accTypelist)
    return jsonify(accTypelist)


@accRecord.route('/newacc', methods=['POST'])
def newacc():
    print("new a accTypepany")
    data = request.get_data()
    print(data)
    json_data = json.loads(data.decode("utf-8"))
    print(json_data)

    if accRecordOprt.newacc(json_data.get('name'), json_data.get('type'),json_data.get('method'),json_data.get('result')):
        return jsonify(1)
    else:
        return jsonify(2)


@accRecord.route('/select', methods=['POST'])
def accTypeVagueSelect():
    data = request.get_data()
    json_data = json.loads(data.decode("utf-8"))
    word = json_data.get('word')
    accTypelist = []
    if accRecordOprt.searchaccType(word) == 0:
        return jsonify(0)
    else:
        for line in accRecordOprt.searchaccType(word):
            accTypelist.append({"acc_id": line[0], "acc_name": line[2],
                               "acc_type": line[3],"acc_method": line[4],"acc_result": line[5],"acc_time": line[1]})
    print(accTypelist)
    return jsonify(accTypelist)


