from flask import Blueprint, jsonify, request, json
import orgMgmt.orgOperation.staffOprt as staffOprt
import datetime

staff = Blueprint('staff', __name__)


@staff.route('/init', methods=['GET'])
def init():
    if staffOprt.getAllStaff() == 0:
        return staffOprt.getAllStaff()
    else:
        stafflist = []
        for line in staffOprt.getAllStaff():
            stafflist.append({"staff_id": line[0], "staff_name": line[1],
                              "staff_phone": line[2]})
        print(stafflist)
    return jsonify(stafflist)


@staff.route('/newstaff', methods=['POST'])
def newstaff():
    print("new a staff")
    data = request.get_data()
    print(data)
    json_data = json.loads(data.decode("utf-8"))

    if staffOprt.newstaff(json_data.get('staff_id'), json_data.get('staff_name'), json_data.get('staff_phone')):
        return jsonify(1)
    else:
        return jsonify(2)


@staff.route('/editstaff', methods=['POST'])
def editstaff():
    print("edit staff")
    data = request.get_data()
    print(data)
    json_data = json.loads(data.decode("utf-8"))

    if staffOprt.editstaff(json_data.get('staff_id'), json_data.get('staff_name'), json_data.get('staff_phone')):
        return jsonify(1)
    else:
        return jsonify(2)


@staff.route('/delete', methods=['POST'])
def staffDelete():
    data = request.get_data()
    print(data)
    word = data.decode("utf-8")
    print(word)
    if staffOprt.deleteStaff(word):
        return jsonify(1)
    else:
        return jsonify(2)


@staff.route('/select', methods=['POST'])
def staffVagueSelect():
    data = request.get_data()
    json_data = json.loads(data.decode("utf-8"))
    word = json_data.get('word')
    stafflist = []
    if staffOprt.searchStaff(word) == 0:
        return jsonify(0)
    else:
        for line in staffOprt.searchStaff(word):
            stafflist.append({"staff_id": line[0], "staff_name": line[1],
                              "staff_phone": line[2]})
        print(stafflist)
        return jsonify(stafflist)
