from flask import Blueprint, jsonify, request, json
import orgMgmt.orgOperation.dpmtOprt as dpmtOprt

dpmt = Blueprint('dpmt', __name__)


@dpmt.route('/', methods=['GET'])
def index():
    dpmtOprt.hello()
    print("here is department")
    return jsonify('departmentttt')


# ###########################################################################################################登录
@dpmt.route('/init', methods=['GET'])
def init():
    if dpmtOprt.getAlldpmt() == 0:
        return dpmtOprt.getAlldpmt()
    else:
        dpmtlist = []
        for line in dpmtOprt.getAlldpmt():  # 对于每一个部门line
            # 如果含-，说明是二级部门，不加入其中
            if "-" in line[0]:
                print("这是一个二级部门" + line[1])
            else:
                subdpmtlist = []
                # 该部门下属部门信息
                if dpmtOprt.searchSubdpmt(line[1]) == 0:
                    subdpmtlist = []
                else:
                    for subdepartment in dpmtOprt.searchSubdpmt(line[1]):
                        subdpmtlist.append({"dpmt_id": subdepartment[0], "dpmt_name": subdepartment[1],
                                            "dpmt_job": subdepartment[2], "job_type": subdepartment[3],
                                            "job_desc": subdepartment[4], "attr_com": subdepartment[5]})
                    print(subdpmtlist)
                # 部门信息
                dpmtlist.append({"dpmt_id": line[0], "dpmt_name": line[1],
                                 "dpmt_job": line[2], "job_type": line[3],
                                 "job_desc": line[4], "attr_com": line[5], "subdpmt": subdpmtlist}, )

        print(dpmtlist)
    return jsonify(dpmtlist)


@dpmt.route('/newdpmt', methods=['POST'])
def newdpmt():
    print("new a department")
    data = request.get_data()
    print(data)
    json_data = json.loads(data.decode("utf-8"))
    print(json_data)

    if dpmtOprt.newdpmt(json_data.get('dpmt_id'), json_data.get('dpmt_name'), json_data.get('dpmt_job'),
                        json_data.get('job_type'), json_data.get('job_desc'), json_data.get('attr_com')):
        return jsonify(1)
    else:
        return jsonify(2)


@dpmt.route('/editdpmt', methods=['POST'])
def editdpmt():
    print("new a edit department")
    data = request.get_data()
    print(data)
    json_data = json.loads(data.decode("utf-8"))
    print(json_data)
    if dpmtOprt.editdpmt(json_data.get('dpmt_id'), json_data.get('dpmt_name'), json_data.get('dpmt_job'),
                         json_data.get('job_type'), json_data.get('job_desc'), json_data.get('attr_com')):
        return jsonify(1)
    else:
        return jsonify(2)


@dpmt.route('/select', methods=['POST'])
def dpmtVagueSelect():
    data = request.get_data()
    json_data = json.loads(data.decode("utf-8"))
    word = json_data.get('word')
    dpmtlist = []
    if dpmtOprt.searchdpmt(word) == 0:
        return jsonify(0)
    else:
        dpmtlist = []
        for line in dpmtOprt.searchdpmt(word):  # 对于查询出的每一个部门line
            subdpmtlist = []
            # 如果含-，说明是二级部门，不加入子部门
            if "-" in line[0]:
                print("这是一个二级部门" + line[1])
                dpmtlist.append({"dpmt_id": line[0], "dpmt_name": line[1],
                                 "dpmt_job": line[2], "job_type": line[3],
                                 "job_desc": line[4], "attr_com": line[5], "subdpmt": subdpmtlist})
            else:
                # 该部门下属部门信息
                if dpmtOprt.searchSubdpmt(line[1]) == 0:
                    subdpmtlist = []
                else:
                    for subdepartment in dpmtOprt.searchSubdpmt(line[1]):
                        subdpmtlist.append({"dpmt_id": subdepartment[0], "dpmt_name": subdepartment[1],
                                            "dpmt_job": subdepartment[2], "job_type": subdepartment[3],
                                            "job_desc": subdepartment[4], "attr_com": subdepartment[5]})
                    print(subdpmtlist)
                # 部门信息
                dpmtlist.append({"dpmt_id": line[0], "dpmt_name": line[1],
                                 "dpmt_job": line[2], "job_type": line[3],
                                 "job_desc": line[4], "attr_com": line[5], "subdpmt": subdpmtlist}, )

        print(dpmtlist)
    return jsonify(dpmtlist)


@dpmt.route('/delete', methods=['POST'])
def dpmtDelete():
    data = request.get_data()
    print(data)
    word = data.decode("utf-8")
    print(word)
    if dpmtOprt.deletedpmt(word):
        return jsonify(1)
    else:
        return jsonify(2)


@dpmt.route('/options', methods=['GET'])
def options():
    # 包括总公司、子公司、一级部门，分两步进行
    print("to get options")

    # 总公司
    options = [{'value': '高梁桥地铁集团',
                'label': '高梁桥地铁集团'}, ]

    # 子公司和一级部门
    if dpmtOprt.getOptions() == 0:
        return jsonify(options)  # 待测试
    else:
        for line in dpmtOprt.getOptions():
            options.append({'value': line[0], 'label': line[0]})
        print(options)
    return jsonify(options)
