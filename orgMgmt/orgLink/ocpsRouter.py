from flask import Blueprint, jsonify, request, json
import orgMgmt.orgOperation.ocpsOprt as ocpsOprt
import orgMgmt.orgOperation.dpmtOprt as dpmtOprt
import datetime

ocps = Blueprint('ocps', __name__)


@ocps.route('/init', methods=['GET'])
def init():
    if dpmtOprt.getAlldpmt() == 0:
        return dpmtOprt.getAlldpmt()
    else:
        departmentlist = []
        for line in dpmtOprt.getAlldpmt():  # 对于每一个部门line
            # ocpsOprt.getOcps(line[0])
            subocpslist = []

            if ocpsOprt.getOcps(line[0]) == 0:
                print("没有人")
            else:
                for line1 in ocpsOprt.getOcps(line[0]):
                    subocpslist.append({"department_id": line1[0], "department_name": line1[1],
                                        "occupa_id": line1[2], "occupa_name": line1[3],
                                        "staff_id": line1[4], "staff_name": line1[5]})
                    print(subocpslist)
            # 部门信息
            departmentlist.append({"dpmt_id": line[0], "dpmt_name": line[1],
                                   "dpmt_job": line[2], "job_type": line[3],
                                   "job_desc": line[4], "attr_com": line[5], "subocps": subocpslist})

        print(departmentlist)
    return jsonify(departmentlist)


@ocps.route('/item_department', methods=['GET'])
def item_department():
    print("get all department")

    if ocpsOprt.getAllDepartment() == 0:
        return ocpsOprt.getAllDepartment()
    else:
        deplist = []
        for line in ocpsOprt.getAllDepartment():
            deplist.append({"dep_id": line[0], "dep_name": line[1]})
        print(deplist)
        return jsonify(deplist)


@ocps.route('/item_occupation', methods=['GET'])
def item_occupation():
    print("get all occupation")

    if ocpsOprt.getAllOccupation() == 0:
        return ocpsOprt.getAllOccupation()
    else:
        ocplist = []
        for line in ocpsOprt.getAllOccupation():
            ocplist.append({"ocp_id": line[0], "ocp_name": line[1]})
        print(ocplist)
        return jsonify(ocplist)


@ocps.route('/item_staff', methods=['GET'])
def item_staff():
    print("get all new staff")

    if ocpsOprt.getStaff() == 0:
        return ocpsOprt.getStaff()
    else:
        stafflist = []
        for line in ocpsOprt.getStaff():
            stafflist.append({"staff_id": line[0], "staff_name": line[1]})
        print(stafflist)
        return jsonify(stafflist)


@ocps.route('/newocps', methods=['POST'])
def newocps():
    print("new ocps")

    data = request.get_data()
    print(data)
    json_data = json.loads(data.decode("utf-8"))

    if ocpsOprt.newocps(json_data.get('department_id'), json_data.get('occupa_id'), json_data.get('staff_id1')):
        return jsonify(1)
    else:
        return jsonify(2)


@ocps.route('/editocps', methods=['POST'])
def editocps():
    print("edit ocps")
    data = request.get_data()
    print(data)
    json_data = json.loads(data.decode("utf-8"))

    if ocpsOprt.editocps(json_data.get('department_id'), json_data.get('occupa_id'),
                         json_data.get('staff_id'), json_data.get('staff_id1')):
        return jsonify(1)
    else:
        return jsonify(2)


@ocps.route('/delete', methods=['POST'])
def ocpsDelete():
    print("delete ocps")
    data = request.get_data()
    print(data)
    json_data = json.loads(data.decode("utf-8"))

    if ocpsOprt.deleteOcps(json_data.get("department_id"), json_data.get("occupa_id"), json_data.get("staff_id")):
        return jsonify(1)
    else:
        return jsonify(2)


@ocps.route('/select', methods=['POST'])
def ocpsVagueSelect():
    data = request.get_data()
    json_data = json.loads(data.decode("utf-8"))
    word = json_data.get('word')
    ocpslist = []
    if ocpsOprt.searchOcps(word) == 0:
        return jsonify(0)
    else:
        for line in ocpsOprt.searchOcps(word):
            ocpslist.append({"department_id": line[0], "department_name": line[1],
                             "occupa_id": line[2], "occupa_name": line[3],
                             "staff_id": line[4], "staff_name": line[5]})
        print(ocpslist)
        return jsonify(ocpslist)
