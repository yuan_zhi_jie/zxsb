from flask import Blueprint, jsonify, request, json
from R_CMgmt.riskCopeOperation import copeAnaOprt

riskCopeAna = Blueprint('riskCopeAna', __name__)


@riskCopeAna.route('/copeAnaIndex', methods=['GET'])
def copeAnaIndex():
    return "1"


@riskCopeAna.route('/init', methods=['GET'])
def init():
    json_str = copeAnaOprt.getAllCopeAna()
    if json_str == 0:
        return json_str

    return jsonify(json_str)


@riskCopeAna.route('/select', methods=['POST'])
def copeAnaVagueSelect():
    data = request.get_data()
    json_data = json.loads(data.decode("utf-8"))
    word = json_data.get('word')

    json_str = copeAnaOprt.searchCopeAna(word)
    if json_str == 0:
        return json_str

    return jsonify(json_str)


@riskCopeAna.route('/addRiskCopeAna', methods=['POST'])
def addRiskCopeAna():
    print("new a RiskCopeTest")
    data = request.get_data()
    print(data)
    json_data = json.loads(data.decode("utf-8"))
    print(json_data)

    if copeAnaOprt.addRiskCopeAnaToDB(json_data.get('risk_cope_record_id'), json_data.get('analysis')):
        return jsonify(1)
    else:
        return jsonify(2)


@riskCopeAna.route('/updateStatusInRiskCope', methods=['POST'])
def updateStatusInRiskCope():
    data = request.get_data()
    print(data)
    json_data = json.loads(data.decode("utf-8"))
    print(json_data)

    if copeAnaOprt.updateStatusInRiskCopeRecordFromDB(json_data.get('risk_cope_record_id')):
        return jsonify(1)
    else:
        return jsonify(2)


@riskCopeAna.route('/getCopeAnaWithCopeID', methods=['POST'])
def getCopeAnaWithCopeID():
    data = request.get_data()
    json_data = json.loads(data.decode("utf-8"))
    risk_cope_id = json_data.get('risk_cope_id')

    json_str = copeAnaOprt.getCopeAnaWithCopeIDFromDB(risk_cope_id)
    if json_str == 0:
        return json_str

    return jsonify(json_str)