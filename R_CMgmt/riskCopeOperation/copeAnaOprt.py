import datetime

import pymysql as sql

connect = sql.Connect(
    host='rm-bp131vjnd9b99vmuq2o.mysql.rds.aliyuncs.com',
    port=3306,
    user='lin_432718',
    passwd='Lin817818',
    db='rail_traffic',
    charset='utf8'
)
cursor = connect.cursor()


def getAllCopeAna():
    print('return all cope_test')

    sql = "select risk_cope_record.risk_cope_record_id, risk_cope_record.time, risk_cope_record.risk_cope_id," \
          "risk_cope.risk_id, riskpoint.name, risk_cope_record.operation, risk_cope_record.user_id " \
          "from risk_cope_record, risk_cope, riskpoint " \
          "where risk_cope_record.risk_cope_id = risk_cope.risk_cope_id " \
          "and risk_cope.risk_id = riskpoint.risk_id " \
          "and risk_cope_record.status = 1"

    print(sql)
    cursor.execute(sql)

    attribute = ['risk_cope_record_id', 'time', 'risk_cope_id', 'risk_id', 'name', 'operation', 'user_id']
    l = []
    for item in cursor.fetchall():
        dic = dict(map(lambda x, y: [x, y], attribute, item))
        dic['time'] = dic['time'].strftime("%Y-%m-%d")
        l.append(dic)

    if l == ():
        print("没有结果")
        return 0
    else:
        print("查询成功")
        return l


def searchCopeAna(word):
    sql = "select risk_cope_record.risk_cope_record_id, risk_cope_record.time, risk_cope_record.risk_cope_id, " \
          "risk_cope.risk_id, riskpoint.name, risk_cope_record.operation, risk_cope_record.user_id " \
          "from risk_cope_record, risk_cope, riskpoint " \
          "where risk_cope_record.risk_cope_id = risk_cope.risk_cope_id " \
          "and risk_cope.risk_id = riskpoint.risk_id " \
          "and risk_cope_record.status = 1 " \
          "and name LIKE '%" + word + "%'"
    print(sql)
    cursor.execute(sql)

    attribute = ['risk_cope_record_id', 'time', 'risk_cope_id', 'risk_id', 'name', 'operation', 'user_id']
    l = []
    for item in cursor.fetchall():
        dic = dict(map(lambda x, y: [x, y], attribute, item))
        dic['time'] = dic['time'].strftime("%Y-%m-%d")
        l.append(dic)

    if l == ():
        print("没有结果")
        return 0
    else:
        print("查询成功")
        return l


def addRiskCopeAnaToDB(risk_cope_record_id, analysis):
    print('insert a new risk cope ana')

    risk_cope_ana_id = newRiskCopeAnaID()

    sql = "insert into risk_cope_ana values('%s', '%s', '%s')" % (str(risk_cope_ana_id), str(risk_cope_record_id),
                                                                  str(analysis))
    print(sql)
    try:
        cursor.execute(sql)
        connect.commit()
    except:
        return False
    else:
        return True


def newRiskCopeAnaID():
    sql = "select max(risk_cope_record_id) from risk_cope_record"
    cursor.execute(sql)
    data = cursor.fetchone()
    print(data)
    number = int(data[0]) + 1
    print(number)
    str = "%03d" % number
    print(str)

    return str


def updateStatusInRiskCopeRecordFromDB(risk_cope_record_id):
    sql = "update risk_cope_record set status = '%d' where risk_cope_record_id = '%s'" % (2, str(risk_cope_record_id))
    print(sql)
    try:
        cursor.execute(sql)
        connect.commit()
    except:
        return False
    else:
        return True


def getCopeAnaWithCopeIDFromDB(risk_cope_id):
    sql = "select risk_cope_ana_id, analysis " \
          "from risk_cope_ana, risk_cope_record " \
          "where risk_cope_ana.risk_cope_record_id = risk_cope_record.risk_cope_record_id " \
          "and risk_cope_record.risk_cope_id = '%s'" % risk_cope_id
    print(sql)
    cursor.execute(sql)

    attribute = ['risk_cope_ana_id', 'analysis']
    l = []
    for item in cursor.fetchall():
        dic = dict(map(lambda x, y: [x, y], attribute, item))
        l.append(dic)

    if l == ():
        print("没有结果")
        return 0
    else:
        print("查询成功")
        return l


